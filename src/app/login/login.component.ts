import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginform:FormGroup;
  selectedIndex="";
  isEditBtnClicked="no";
  loginList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.loginform=this.formBuilder.group(
        {
            loginemail:['',[Validators.required]],
            password:['',[Validators.required]],
            
        }

      )
  }
  
    submit(){
      this.isSubmitted = true;
    if(this.loginform.valid)
    {
      console.log('submit',this.loginform.value);
      // this.loginList.push(this.doctorform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('loginList',JSON.stringify(this.loginList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.loginform.reset();
      this.isSubmitted = false;
    }
      
    }
  clear(){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, cancel it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          'Deleted!',
          'Your details has been deleted.',
          'success'
        )
        this.loginform.reset();
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your details are safe :)',
          'error'
        )
      }
    })
    
    
  }

  ngOnInit(): void {
  // Code to read data from localstorage
  let data = localStorage.getItem('loginList');
  this.loginList = JSON.parse(data || '');
  }
}
