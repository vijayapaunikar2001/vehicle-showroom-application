import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mini2';
  // Login
  loginemail="";
  password="";
  selectedIndex="";
  isEditBtnClicked="no";
  loginList:any=[]

  submit(){
    let login ={
      loginemail:this.loginemail,
      password:this.password
    }
    this.loginList.push(login)
    this.clear()
    console.log("Successfully Loggined",login);
  }

  clear(){
    this.loginemail="";
    this.password="";
  }

  edit(idx:any){
    this.isEditBtnClicked="yes";
    this.selectedIndex = idx;
    this.loginemail=this.loginList[idx].loginemail;
    this.password=this.loginList[idx].password;

  }

  delete(idx:any){
    this.loginList.splice(idx,1);
  }

  update(){
    this.loginList[this.selectedIndex].loginemail = this.loginemail;
    this.loginList[this.selectedIndex].password = this.password;
    this.clear();
    this.isEditBtnClicked="no";
  }
  // Register
  registerList:any=[];
  rname="";
  rcontact="";
  raddress="";
  remail="";
  selectedIndex1="";
  isEditBtnClicked1="no";

  submit1(){
    let register ={
      rname:this.rname,
      rcontact:this.rcontact,
      raddress:this.raddress,
      remail:this.remail,
    }
    this.registerList.push(register)
    this.clear1()
    console.log("Successfully Registered",register);
  }

  clear1(){
    this.rname="";
    this.remail="";
    this.rcontact="";
    this.raddress="";
  }
  edit1(idx:any){
    this.isEditBtnClicked1="yes";
    this.selectedIndex1 = idx;
    this.rname=this.registerList[idx].rname;
    this.remail=this.registerList[idx].remail;
    this.rcontact=this.registerList[idx].rcontact;
    this.raddress=this.registerList[idx].raddress;
  }

  delete1(idx:any){
    this.registerList.splice(idx,1);
  }

  update1(){
    this.registerList[this.selectedIndex1].rname = this.rname;
    this.registerList[this.selectedIndex1].remail = this.remail;
    this.registerList[this.selectedIndex1].rcontact = this.rcontact;
    this.registerList[this.selectedIndex1].raddress = this.raddress;
    
    this.clear1();
    this.isEditBtnClicked1="no";
  }

  // Cars
  carList:any=[];
  cmodel="";
  creg="";
  cage="";
  cengine="";
  selectedIndex2="";
  isEditBtnClicked2="no";

  submit2(){
    let car ={
      cmodel:this.cmodel,
      creg:this.creg,
      cage:this.cage,
      cengine:this.cengine,
    }
    this.carList.push(car)
    this.clear2()
    
  }

  clear2(){
    this.cmodel="";
    this.creg="";
    this.cage="";
    this.cengine="";
  }
  edit2(idx:any){
    this.isEditBtnClicked2="yes";
    this.selectedIndex2 = idx;
    this.cmodel=this.carList[idx].cmodel;
    this.creg=this.carList[idx].creg;
    this.cage=this.carList[idx].cage;
    this.cengine=this.carList[idx].cengine;
    
  }
 
  delete2(idx:any){
    this.carList.splice(idx,1);
  }

  update2(){
    this.carList[this.selectedIndex2].cmodel = this.cmodel;
    this.carList[this.selectedIndex2].creg = this.creg;
    this.carList[this.selectedIndex2].cage = this.cage;
    this.carList[this.selectedIndex2].cengine = this.cengine;
    
    this.clear2();
    this.isEditBtnClicked2="no";
  }
// Parts
partsList:any=[];
pmodel="";
pchasis="";
pbody="";
pengine="";
selectedIndex3="";
isEditBtnClicked3="no";

submit3(){
  let parts ={
    pmodel:this.pmodel,
    pchasis:this.pchasis,
    pbody:this.pbody,
    pengine:this.pengine,
  }
  this.partsList.push(parts)
  this.clear3()
  
}

clear3(){
  this.pmodel="";
  this.pchasis="";
  this.pbody="";
  this.pengine="";
}
edit3(idx:any){
  this.isEditBtnClicked3="yes";
  this.selectedIndex3 = idx;
  this.pmodel=this.partsList[idx].pmodel;
  this.pchasis=this.partsList[idx].pchasis;
  this.pbody=this.partsList[idx].pbody;
  this.pengine=this.partsList[idx].pengine;
}

delete3(idx:any){
  this.partsList.splice(idx,1);
}

update3(){
  this.partsList[this.selectedIndex3].pmodel = this.pmodel;
  this.partsList[this.selectedIndex3].pchasis = this.pchasis;
  this.partsList[this.selectedIndex3].pbody = this.pbody;
  this.partsList[this.selectedIndex3].pengine = this.pengine;
  
  this.clear3();
  this.isEditBtnClicked3="no";
}
// Staff
staffList:any=[];
  sname="";
  semail="";
  scontact="";
  saddress="";
  selectedIndex4="";
  isEditBtnClicked4="no";

  submit4(){
    let staff ={
      sname:this.sname,
      semail:this.semail,
      scontact:this.scontact,
      saddress:this.saddress,
    }
    this.staffList.push(staff)
    this.clear4()
    
  }

  clear4(){
    this.sname="";
    this.semail="";
    this.scontact="";
    this.saddress="";
  }
  edit4(idx:any){
    this.isEditBtnClicked4="yes";
    this.selectedIndex4 = idx;
    this.sname=this.staffList[idx].sname;
    this.semail=this.staffList[idx].semail;
    this.scontact=this.staffList[idx].scontact;
    this.saddress=this.staffList[idx].saddress;
  }

  delete4(idx:any){
    this.staffList.splice(idx,1);
  }

  update4(){
    this.staffList[this.selectedIndex4].sname = this.sname;
    this.staffList[this.selectedIndex4].semail = this.semail;
    this.staffList[this.selectedIndex4].scontact = this.scontact;
    this.staffList[this.selectedIndex4].saddress = this.saddress;
    
    this.clear4();
    this.isEditBtnClicked4="no";
  }
  // Bill
  billList:any=[];
  bmodel="";
  breg="";
  bdate="";
  bamount="";
  selectedIndex5="";
  isEditBtnClicked5="no";

  submit5(){
    let bill ={
      bmodel:this.bmodel,
      breg:this.breg,
      bdate:this.bdate,
      bamount:this.bamount,
    }
    this.billList.push(bill)
    this.clear5()
    
  }

  clear5(){
    this.bmodel="";
    this.breg="";
    this.bdate="";
    this.bamount="";
  }
  edit5(idx:any){
    this.isEditBtnClicked5="yes";
    this.selectedIndex5 = idx;
    this.bmodel=this.billList[idx].bmodel;
    this.breg=this.billList[idx].breg;
    this.bdate=this.billList[idx].bdate;
    this.bamount=this.billList[idx].bamount;
  }

  delete5(idx:any){
    this.billList.splice(idx,1);
  }

  update5(){
    this.staffList[this.selectedIndex5].bmodel = this.bmodel;
    this.staffList[this.selectedIndex5].breg = this.breg;
    this.staffList[this.selectedIndex5].bdate = this.bdate;
    this.staffList[this.selectedIndex5].bamount = this.bamount;
    this.clear5();
    this.isEditBtnClicked5="no";
  }


  
}
