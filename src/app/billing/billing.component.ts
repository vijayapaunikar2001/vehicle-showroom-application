import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {

  billingform:FormGroup;
  selectedIndex5="";
  isEditBtnClicked5="no";
  billList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.billingform=this.formBuilder.group(
        {
            bmodel:['',[Validators.required]],
            breg:['',[Validators.required]],
            bdate:['',[Validators.required]],
            bamount:['',[Validators.required]],
        }

      )
  }
  
    submit5(){
      this.isSubmitted = true;
    if(this.billingform.valid)
    {
      console.log('submit',this.billingform.value);
      this.billList.push(this.billingform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('billList',JSON.stringify(this.billList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.billingform.reset();
      this.isSubmitted = false;
    }
      
    }
    clear5(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.billingform.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit5(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked5="yes";
      this.selectedIndex5 = idx;
      this.billingform.patchValue(
        {
          bmodel: this.billList[idx].bmodel,
          breg: this.billList[idx].breg,
          bdate: this.billList[idx].bdate,
          bamount: this.billList[idx].bamount,
        }
      )
    }
  
    delete5(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.billList.splice(idx,1);
          // Code to update data in localstorage
      localStorage.setItem('billList',JSON.stringify(this.billList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update5(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.billList[this.selectedIndex5].bmodel = this.billingform.value.bmodel;
      this.billList[this.selectedIndex5].breg = this.billingform.value.breg;
      this.billList[this.selectedIndex5].bdate = this.billingform.value.bdate;
      this.billList[this.selectedIndex5].bamount = this.billingform.value.bamount;
      // Code to update data in localstorage
      localStorage.setItem('billList',JSON.stringify(this.billList))
      this.billingform.reset();
      this.isEditBtnClicked5="no";
    }
    ngOnInit(): void {
      // Code to read data from localstorage
      let data = localStorage.getItem('billList');
      this.billList = JSON.parse(data || '');  
    }
  
    
}
