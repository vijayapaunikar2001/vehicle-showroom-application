import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-parts',
  templateUrl: './parts.component.html',
  styleUrls: ['./parts.component.scss']
})
export class PartsComponent implements OnInit {


partsform:FormGroup;
  selectedIndex3="";
  isEditBtnClicked3="no";
  partsList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.partsform=this.formBuilder.group(
        {
          pmodel:['',[Validators.required]],
          pchasis:['',[Validators.required]],
          pbody:['',[Validators.required]],
          pengine:['',[Validators.required]], 
        }

      )
  }
  
    submit3(){
      this.isSubmitted = true;
    if(this.partsform.valid)
    {
      console.log('submit',this.partsform.value);
      this.partsList.push(this.partsform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('carList',JSON.stringify(this.partsList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.partsform.reset();
      this.isSubmitted = false;
    }
      
    }
    clear3(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.partsform.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit3(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked3="yes";
      this.selectedIndex3 = idx;
      this.partsform.patchValue(
        {
          pmodel: this.partsList[idx].pmodel,
          pchasis: this.partsList[idx].pchasis,
          pbody: this.partsList[idx].pbody,
          pengine: this.partsList[idx].pengine,
        }
      )
    }
  
    delete3(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.partsList.splice(idx,1);
          // Code to update data in localstorage
      localStorage.setItem('partsList',JSON.stringify(this.partsList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update3(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.partsList[this.selectedIndex3].pmodel = this.partsform.value.pmodel;
      this.partsList[this.selectedIndex3].pchasis = this.partsform.value.pchasis;
      this.partsList[this.selectedIndex3].pbody = this.partsform.value.pbody;
      this.partsList[this.selectedIndex3].pengine = this.partsform.value.pengine;
      // Code to update data in localstorage
      localStorage.setItem('partsList',JSON.stringify(this.partsList))
      this.partsform.reset();
      this.isEditBtnClicked3="no";
    }
    ngOnInit(): void {
      // Code to read data from localstorage
      let data = localStorage.getItem('partsList');
      this.partsList = JSON.parse(data || '');

    }
  
    
}

