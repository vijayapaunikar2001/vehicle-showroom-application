import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit {


staffform:FormGroup;
  selectedIndex4="";
  isEditBtnClicked4="no";
  staffList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.staffform=this.formBuilder.group(
        {
          sname:['',[Validators.required]],
          semail:['',[Validators.required]],
          scontact:['',[Validators.required]],
          saddress:['',[Validators.required]], 
        }

      )
  }
  
    submit4(){
      this.isSubmitted = true;
    if(this.staffform.valid)
    {
      console.log('submit',this.staffform.value);
      this.staffList.push(this.staffform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('carList',JSON.stringify(this.staffList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.staffform.reset();
      this.isSubmitted = false;
    }
      
    }
    clear4(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.staffform.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit4(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked4="yes";
      this.selectedIndex4 = idx;
      this.staffform.patchValue(
        {
          pmodel: this.staffList[idx].pmodel,
          pchasis: this.staffList[idx].pchasis,
          pbody: this.staffList[idx].pbody,
          pengine: this.staffList[idx].pengine,
        }
      )
    }
  
    delete4(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.staffList.splice(idx,1);
           // Code to update data in localstorage
      localStorage.setItem('staffList',JSON.stringify(this.staffList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update4(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.staffList[this.selectedIndex4].sname = this.staffform.value.sname;
      this.staffList[this.selectedIndex4].semail = this.staffform.value.semail;
      this.staffList[this.selectedIndex4].scontact = this.staffform.value.scontact;
      this.staffList[this.selectedIndex4].saddress = this.staffform.value.saddress;
      // Code to update data in localstorage
      localStorage.setItem('staffList',JSON.stringify(this.staffList))
      this.staffform.reset();
      this.isEditBtnClicked4="no";
    }
    ngOnInit(): void {
        // Code to read data from localstorage
       let data = localStorage.getItem('staffList');
       this.staffList = JSON.parse(data || '');

    }
  
    
}

