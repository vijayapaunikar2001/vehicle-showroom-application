import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingComponent } from './billing/billing.component';
import { CarComponent } from './car/car.component';
import { LoginComponent } from './login/login.component';
import { PartsComponent } from './parts/parts.component';
import { RegisterComponent } from './register/register.component';
import { StaffComponent } from './staff/staff.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'login',
    pathMatch:'full'
  },
  {
    path:'parts',
    component:PartsComponent
  },
  {
    path:'cars',
    component:CarComponent
  },
  {
    path:'staff',
    component:StaffComponent
  },
  {
    path:'billing',
    component:BillingComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
