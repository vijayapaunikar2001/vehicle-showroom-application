import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerform:FormGroup;
  selectedIndex1="";
  isEditBtnClicked1="no";
  registerList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.registerform=this.formBuilder.group(
        {
            rname:['',[Validators.required]],
            remail:['',[Validators.required]],
            rcontact:['',[Validators.required]],
            raddress:['',[Validators.required]],
        }

      )
  }
  
    submit1(){
      this.isSubmitted = true;
    if(this.registerform.valid)
    {
      console.log('submit',this.registerform.value);
      this.registerList.push(this.registerform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('doctorList',JSON.stringify(this.registerList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.registerform.reset();
      this.isSubmitted = false;
    }
      
    }
  clear1(){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, cancel it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          'Deleted!',
          'Your details has been deleted.',
          'success'
        )
        this.registerform.reset();
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your details are safe :)',
          'error'
        )
      }
    })
    
    
  }

  ngOnInit(): void {
    // Code to read data from localstorage
    let data = localStorage.getItem('registerList');
    this.registerList = JSON.parse(data || '');
  }
}
