import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {

  carform:FormGroup;
  selectedIndex2="";
  isEditBtnClicked2="no";
  carList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.carform=this.formBuilder.group(
        {
          cmodel:['',[Validators.required]],
          creg:['',[Validators.required]],
          cage:['',[Validators.required]],
          cengine:['',[Validators.required]], 
        }

      )
  }
  
    submit2(){
      this.isSubmitted = true;
    if(this.carform.valid)
    {
      console.log('submit',this.carform.value);
      this.carList.push(this.carform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('carList',JSON.stringify(this.carList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.carform.reset();
      this.isSubmitted = false;
    }
      
    }
    clear2(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.carform.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit2(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked2="yes";
      this.selectedIndex2 = idx;
      this.carform.patchValue(
        {
          cmodel: this.carList[idx].cmodel,
          creg: this.carList[idx].creg,
          cage: this.carList[idx].cage,
          cengine: this.carList[idx].cengine,
        }
      )
    }
  
    delete2(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.carList.splice(idx,1);
          // Code to update data in localstorage
      localStorage.setItem('carList',JSON.stringify(this.carList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update2(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.carList[this.selectedIndex2].cmodel = this.carform.value.cmodel;
      this.carList[this.selectedIndex2].creg = this.carform.value.creg;
      this.carList[this.selectedIndex2].cage = this.carform.value.cage;
      this.carList[this.selectedIndex2].cengine = this.carform.value.cengine;
      // Code to update data in localstorage
      localStorage.setItem('carList',JSON.stringify(this.carList))
      this.carform.reset();
      this.isEditBtnClicked2="no";
    }
    ngOnInit(): void {
        // Code to read data from localstorage
       let data = localStorage.getItem('carList');
       this.carList = JSON.parse(data || '');
    }
  
    
}
